import java.util.Random;
import java.util.Scanner;

public class Main {
    public static int sumRange(int from, int to, int[] a) {
        int sum = 0;
        if (a[0] > a[a.length-1]) {
            return -1;
        }
        for (int i = from; i <= to; i++) {
            sum += a[i];
        }
        return sum;
    }
    public static void isEvenOfArray(int[] array) {
        for (int j : array) {
            if (j % 2 == 0) {
                System.out.println(j);
            }
        }
    }
    public static void main(String[] args) {
        int[] array = {1, 5, 7, 2, 3};
        int sum1 = sumRange(0, 4, array);
        System.out.println(sum1);
        isEvenOfArray(array);
    }
}
