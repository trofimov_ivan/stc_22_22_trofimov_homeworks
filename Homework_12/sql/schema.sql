create table user_homework
(
    id         bigserial primary key,
    first_name varchar(20) default 'DEFAULT_FIRSTNAME',
    last_name  varchar(20) default 'DEFAULT_LASTNAME',
    number_phone varchar(20) not null ,
    experience integer check (experience >= 0),
    age        integer check (age >= 0 and age <= 120) not null,
    driving_licence  bool,
    category_licence varchar(20),
    rating integer check ( rating >= 0 and rating <= 5 )
);

insert into user_homework (first_name, last_name, number_phone, experience, age, driving_licence, category_licence,
                           rating)
values ('Иван','Трофимов','+79879781062', 5, 26, true, 'B', 1),
       ('Григорий','Хахулин','+79879781545', 0, 40, false, 'отсутствует', 0),
       ('Вечаслав','Тягунов','+79879785488', 2, 20, true, 'B', 3),
       ('Дмитрий','Быков','+79879784686', 0, 31, false, 'отсутствует', 0),
       ('Олег','Пронин','+79879788424', 6, 24, true, 'B', 5);

create table car
(
    id bigserial primary key,
    model varchar(20),
    color varchar(20),
    number_car varchar(20),
    user_id integer not null,
    foreign key (user_id) references user_homework (id)
);

insert into car (model, color, number_car, user_id)
values ('Mirage', 'белый', 'Е899МО_763', 1),
       ('Prada', 'черный', 'Н777НН_763', 2),
       ('ВАЗ_2101', 'синий', 'А654НЕ_05', 3),
       ('Prius', 'голубой', 'К123ОМ_125', 4),
       ('Fit', 'розовый', 'Е432СО_125', 5);

create table ride (
                      driver_id integer not null,
                      foreign key (driver_id) references user_homework(id),
                      car_id integer not null,
                      foreign key (car_id) references car(id),
                      travel_date timestamp,
                      time_ride time
);
insert into ride (driver_id, car_id, travel_date, time_ride)
values (1, 1, '2022-11-15','5:00'),
       (3, 2, '2022-11-15','24:00'),
       (3, 3, '2022-11-15','2:00'),
       (5, 4, '2022-11-15','7:00'),
       (5, 5, '2022-11-15','6:30');