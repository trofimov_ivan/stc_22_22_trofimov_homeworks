package ru.inno.cars.services;

import lombok.RequiredArgsConstructor;
import ru.inno.cars.models.Car;
import ru.inno.cars.repository.CarRepository;

import java.util.Scanner;

@RequiredArgsConstructor
public class CarServices {
    public final CarRepository carRepository;

    public void write(Car car){

        carRepository.save(car);

    }

}
