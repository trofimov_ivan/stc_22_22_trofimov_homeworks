package ru.inno.cars.repository.impl;

import ru.inno.cars.models.Car;
import ru.inno.cars.repository.CarRepository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;




public class CarRepositoryJdbcImpl implements CarRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from cars order by id";

    //language=SQL
    private static final String SQL_INSERT = "insert into  cars( model," +
            " color, number) values (?, ?, ?)";
    private DataSource dataSource;

    public CarRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private final static Function<ResultSet, Car> carRowMapper = row -> {
        try {

            return Car.builder()
                    .id(row.getLong("id"))
                    .model(row.getString("model"))
                    .color(row.getString("color"))
                    .number(row.getString("number"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    };


    @Override
    public List<Car> findAll() {
        List<Car> cars = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL)) {
                while (resultSet.next()) {
                    Car car = carRowMapper.apply(resultSet);
                    cars.add(car);
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return cars;
    }

    @Override
    public void save(Car car) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT,
                     Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, car.getModel());
            preparedStatement.setString(2, car.getColor());
            preparedStatement.setString(3, car.getNumber());
            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert car");
            }
            try (ResultSet generatedId = preparedStatement.getGeneratedKeys()) {
                if (generatedId.next()) {
                    car.setId(generatedId.getLong("id"));
                } else {
                    throw new SQLException("Can't obtain generated id");
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

}
