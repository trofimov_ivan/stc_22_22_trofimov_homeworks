package ru.inno.cars.app;


import com.beust.jcommander.Parameters;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.cars.models.Car;
import ru.inno.cars.repository.CarRepository;
import ru.inno.cars.repository.impl.CarRepositoryJdbcImpl;
import ru.inno.cars.services.CarServices;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

@Parameters(separators = "|")
public class Main {

    public static void main(String[] args) {
        String action = args[0];
        Properties dbProperties = new Properties();

        try {
            dbProperties.load(new BufferedReader(
                    new InputStreamReader(Main.class.getResourceAsStream("/db.properties"))));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setPassword(dbProperties.getProperty("db.password"));
        dataSource.setUsername(dbProperties.getProperty("db.username"));
        dataSource.setJdbcUrl(dbProperties.getProperty("db.url"));
        dataSource.setMaximumPoolSize(Integer.parseInt(dbProperties.getProperty("db.hikari.maxPoolSize")));
        CarRepository carRepository = new CarRepositoryJdbcImpl(dataSource);
        CarServices carServices = new CarServices(carRepository);
        if (action.equals("read")) {
            List<Car> cars = carRepository.findAll();
            for (Car car : cars) {
                System.out.println(car);
            }
        } else if (action.equals("write")) {
            Scanner scanner = new Scanner(System.in);
            while (true) {
                System.out.println("Model");
                String model = scanner.nextLine();
                System.out.println("Color");
                String color = scanner.nextLine();
                System.out.println("number");
                String number = scanner.nextLine();
                Car car = Car.builder()
                        .model(model)
                        .color(color)
                        .number(number)
                        .build();
                carServices.write(car);
                String newCar = scanner.nextLine();
                if (newCar.equalsIgnoreCase("exit")) {
                    return;
                }
            }
        }
    }
}

