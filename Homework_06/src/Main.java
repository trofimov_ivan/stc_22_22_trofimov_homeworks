public class Main {

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(5,5,8, 3);
        Square square = new Square(5);
        Circle circle = new Circle(4);
        Ellipse ellipse = new Ellipse(5, 10);
        Figure[] figures = new Figure[]{square, circle, ellipse, rectangle};
        completeAll(figures);
        }
        public static void completeAll(Figure[] figures) {
            for (Figure figure : figures) {
                System.out.println(figure.area());
                System.out.println(figure.perimeter());
                System.out.println(figure.toString());
            }
        }
    }
