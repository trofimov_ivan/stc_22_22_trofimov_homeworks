import static java.lang.Math.PI;
public class Ellipse extends Figure {
    @Override
    public String toString() {
        return "Ellipse{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    private int minRadius = 5;
    private int maxRadius = 10;
    public Ellipse(int minRadius, int maxRadius) {
        this.minRadius = minRadius;
        this.maxRadius = maxRadius;
    }
    public double perimeter() {
        return (4*PI*minRadius*maxRadius+(maxRadius-minRadius))/(maxRadius+minRadius);
    }
    public double area() {
       return PI*minRadius*maxRadius;
    }
}
