public abstract class Figure {
    protected int x;
    protected int y;
    public  void move(int toX, int toY) {
        this.x = toX;
        this.y = toY;
    }
    abstract public double perimeter();

    abstract public double area();
}
