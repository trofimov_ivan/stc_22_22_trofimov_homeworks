public class Rectangle extends Figure {
    private int width = 3;
    private int height = 8;

    @Override
    public String toString() {
        return "Rectangle{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public Rectangle(int x, int y, int height, int width) {
        move( x, y);
        this.height = height;
        this.width = width;
    }
    public double perimeter() {
        return 2*width+2*height;
    }
    public double area() {
        return height*width;
    }
}
