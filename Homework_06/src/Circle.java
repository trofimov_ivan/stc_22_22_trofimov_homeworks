import static java.lang.Math.PI;
public class Circle extends Figure{
    private int radius = 4;

    @Override
    public String toString() {
        return "Circle{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public Circle(int radius) {
        this.radius = radius;
    }
    public double perimeter() {
        return 2*PI*radius;
    }
    public double area() {
    return PI*radius*radius;
    }
}
