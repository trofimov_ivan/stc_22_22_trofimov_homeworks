public class Square extends Figure {
    private int sideLong = 5;

    @Override
    public String toString() {
        return "Square{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public Square(int sideLong) {

        this.sideLong = sideLong;
    }
    public double perimeter() {
        return 4*sideLong;
    }
    public double area() {
        return sideLong*sideLong;
    }
}
