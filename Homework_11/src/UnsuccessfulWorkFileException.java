public class UnsuccessfulWorkFileException extends RuntimeException{
    public UnsuccessfulWorkFileException(String message) {
        super(message);
    }
}
