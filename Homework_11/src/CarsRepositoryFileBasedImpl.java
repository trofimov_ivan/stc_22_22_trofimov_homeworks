import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CarsRepositoryFileBasedImpl implements CarsRepository {
    private final String fileName;

    private static final Function<String, Car> stringToCarMapper = currentCar -> {
        String[] parts = currentCar.split("\\|");
        String numberCar = parts[0];
        String model = parts[1];
        String color = parts[2];
        Integer mileage = Integer.parseInt(parts[3]);
        Integer price = Integer.parseInt(parts[4]);
        return new Car(numberCar, model, color, mileage, price);
    };
    private final Function<Car, String> carToStringMapper = car ->
            car.getNumberCar() + "|"
                    + car.getModel() + "|" + car.getColor() + "|"
                    + car.getMileage() + "|" + car.getPrice();

    public CarsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public double findAverageCarPrice() {
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getModel().equals("Camry"))
                    .mapToInt(Car::getPrice)
                    .average()
                    .getAsDouble();
        } catch (IOException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public String minPriceOfColor() {
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .min(Comparator.comparingInt(Car::getPrice))
                    .map(Car::getColor)
                    .orElseThrow(() -> new UnsuccessfulWorkFileException("Ошибка. Null"));
        } catch (IOException e) {
            throw new UnsupportedOperationException(e);
        }
        //● Номера всех автомобилей, имеющих черный цвет или нулевой пробег.
//● Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.
    }

    @Override
    public List<String> numberAllCars(String color1, Integer mileage1) {
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            return bufferedReader
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getColor().equals(color1) || car.getMileage().equals(mileage1))
                    .map(Car::getNumberCar)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccessfulWorkFileException("Ошибка!");
        }
    }

    @Override
    public long findQuantityUniqueModel() {
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            return bufferedReader
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getPrice() >= 700_000 && car.getPrice() <= 800_000)
                    .map(Car::getModel)
                    .distinct()
                    .count();
        } catch (IOException e) {
            throw new UnsuccessfulWorkFileException("Ошибка!");
        }
    }
}

