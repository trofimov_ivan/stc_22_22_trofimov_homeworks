import java.util.List;

public interface CarsRepository {
    double findAverageCarPrice();
    String minPriceOfColor();
    List<String> numberAllCars(String color1, Integer mileage1);
    long findQuantityUniqueModel();



}
