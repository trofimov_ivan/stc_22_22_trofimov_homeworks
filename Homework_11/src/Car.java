public class Car {
    private String numberCar;
    private String model;
    private String color;
    private Integer mileage;
    private Integer price;

    public Car(String numberCar, String model, String color, Integer mileage, Integer prise) {
        this.numberCar = numberCar;
        this.model = model;
        this.color = color;
        this.mileage = mileage;
        this.price = prise;
    }

    public String getNumberCar() {
        return numberCar;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public Integer getMileage() {
        return mileage;
    }

    public Integer getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "numberCar=" + numberCar +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", mileage=" + mileage +
                ", prise=" + price +
                '}';
    }
}
