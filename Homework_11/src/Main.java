//Предусмотреть следующие методы в репозитории, позволяющие получить:
//● Номера всех автомобилей, имеющих черный цвет или нулевой пробег.
//● Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.
//● Вывести цвет автомобиля с минимальной стоимостью
//● Среднюю стоимость
public class Main {
    public static void main(String[] args) {
        CarsRepository carsRepository = new CarsRepositoryFileBasedImpl("input.txt");
        System.out.println(carsRepository.findAverageCarPrice());
        System.out.println(carsRepository.minPriceOfColor());
        System.out.println(carsRepository.numberAllCars("Black", 0));
        System.out.println(carsRepository.findQuantityUniqueModel());
    }
}