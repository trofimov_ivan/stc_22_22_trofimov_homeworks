public class ATM {
    private int sumRemainsMoney;
    private int sumMaxMoney;
    private int maxMoneyAtm;
    private int numberOfOperations;

    public ATM(int maxMoneyAtm, int sumRemainsMoney, int sumMaxMoney) {
        this.maxMoneyAtm = maxMoneyAtm;
    }

    public void setSumMaxMoney(int sumMaxMoney) {
        if (sumMaxMoney >= 0 && sumMaxMoney <= 1_000_000) {
            this.sumMaxMoney = sumMaxMoney;
        } else {
            this.sumMaxMoney = 0;
        }
    }

    public int withdrawalOfCash(int cash) {
        if (cash <= sumMaxMoney && cash <= sumRemainsMoney) {
            return cash;
        }
        return 0;
    }

    public int putMoney(int put) {
        if (sumRemainsMoney + put > maxMoneyAtm) {
            int remains = sumRemainsMoney + put - maxMoneyAtm;
            sumRemainsMoney = maxMoneyAtm - put + remains;
            return remains;
        }
        sumRemainsMoney = sumRemainsMoney + put;
        return sumRemainsMoney;
    }

    public int getSumRemainsMoney() {
        return sumRemainsMoney;
    }

    public int getSumMaxMoney() {
        return sumMaxMoney;
    }

    public int getMaxMoneyAtm() {
        return maxMoneyAtm;
    }

    public int getNumberOfOperations() {
        return numberOfOperations;
    }

    public void setSumRemainsMoney(int sumRemainsMoney) {
        this.sumRemainsMoney = sumRemainsMoney;
    }

    public void setMaxMoneyAtm(int maxMoneyAtm) {
        this.maxMoneyAtm = maxMoneyAtm;
    }

    public void setNumberOfOperations(int numberOfOperations) {
        this.numberOfOperations = numberOfOperations;
    }
}