package ru.inno.fa.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class SessionForm {
    private String description;
    private LocalTime beginning;
    private LocalTime finish;
}
