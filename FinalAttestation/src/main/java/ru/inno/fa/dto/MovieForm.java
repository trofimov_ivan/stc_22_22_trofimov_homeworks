package ru.inno.fa.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MovieForm {
    private String name;
    private String description;
    private Integer price;
    private LocalTime startTime;
    private LocalTime finishTime;

}
