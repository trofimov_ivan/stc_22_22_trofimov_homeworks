package ru.inno.fa.models;



import lombok.*;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(exclude = {"session"})
@ToString(exclude = {"session"})
public class Movie {
    public enum State {
        CONFIRMED, DELETED
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    @Column(length = 1000)
    private String description;
    private Integer price;

    @Column(name = "start_time")
    private LocalTime startTime;
    @Column(name = "finish_time")
    private LocalTime finishTime;

    @ManyToOne()
    @JoinColumn(name = "session_id")
    private Session session;

    @Enumerated(value = EnumType.STRING)
    private Movie.State state;

    @ManyToMany
    @JoinTable(joinColumns = {@JoinColumn(name = "movie_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "session_id", referencedColumnName = "id")})
    private Set<Session> sessions;
}
