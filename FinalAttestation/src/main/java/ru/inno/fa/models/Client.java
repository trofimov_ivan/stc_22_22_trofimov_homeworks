package ru.inno.fa.models;

import lombok.*;


import javax.persistence.*;
import java.util.Set;
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"sessions"})
@ToString(exclude = {"sessions"})
@Builder
@Entity
@Table(name = "client_cinema")
public class Client {

    public enum State {
        NOT_CONFIRMED, CONFIRMED, DELETED
    }
    public enum Role {
        USER, ADMIN
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String email;
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;


    @ManyToMany
    @JoinTable(joinColumns = {@JoinColumn(name = "client_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "session_id", referencedColumnName = "id")})
    private Set<Session> sessions;


    @Enumerated(value = EnumType.STRING)
    private State state;

    @Enumerated(value = EnumType.STRING)
    private Role role;
}

