package ru.inno.fa.models;



import lombok.*;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.Set;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(exclude = {"movies", "clients"})
public class Session {
    public enum State {
        CONFIRMED, DELETED
    }
    @Enumerated(value = EnumType.STRING)
    private Session.State state;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(length = 1000)
    private String description;

    private LocalTime beginning;
    private LocalTime finish;

    @OneToMany(mappedBy = "session", fetch = FetchType.EAGER)
    private Set<Movie> movies;

    @ManyToMany(mappedBy = "sessions", fetch = FetchType.EAGER)
    private Set<Client> clients;
}
