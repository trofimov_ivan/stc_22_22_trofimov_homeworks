package ru.inno.fa.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.fa.dto.SessionForm;
import ru.inno.fa.models.Client;
import ru.inno.fa.models.Movie;
import ru.inno.fa.models.Session;
import ru.inno.fa.repositories.SessionsRepository;
import ru.inno.fa.repositories.MoviesRepository;
import ru.inno.fa.repositories.ClientsRepository;
import ru.inno.fa.services.SessionsService;

import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
@Service
public class SessionsServiceImpl implements SessionsService {
    private final SessionsRepository sessionsRepository;
    private final ClientsRepository clientsRepository;
    private final MoviesRepository moviesRepository;
    @Override
    public void addClientToSession(Long sessionId, Long clientId) {
        Session session = sessionsRepository.findById(sessionId).orElseThrow();
        Client client = clientsRepository.findById(clientId).orElseThrow();

        client.getSessions().add(session);

        clientsRepository.save(client);

    }

    @Override
    public Session getSession(Long sessionId) {
        return sessionsRepository.findById(sessionId).orElseThrow();
    }

    @Override
    public List<Client> getNotInSessionClients(Long sessionId) {
        Session session = sessionsRepository.findById(sessionId).orElseThrow();
        return clientsRepository.findAllBySessionsNotContains(session);
    }

    @Override
    public List<Movie> getNotInSessionMovies(Long sessionId) {
        Session session = sessionsRepository.findById(sessionId).orElseThrow();
        return moviesRepository.findAllBySessionsNotContains(session);
    }

    @Override
    public List<Client> getInSessionClients(Long sessionId) {
        Session session = sessionsRepository.findById(sessionId).orElseThrow();
        return clientsRepository.findAllBySessionsContains(session);

    }

    @Override
    public List<Movie> getInSessionMovies(Long sessionId) {
        Session session = sessionsRepository.findById(sessionId).orElseThrow();
        return moviesRepository.findAllBySessionsContains(session);
    }

    @Override
    public void addSession(SessionForm session) {
        Session newSession = Session.builder()
                .description(session.getDescription())
                .beginning(session.getBeginning())
                .finish(session.getFinish())
                .state(Session.State.CONFIRMED)
                .build();

        sessionsRepository.save(newSession);
    }

    @Override
    public List<Session> getAllSessions() {
        return sessionsRepository.findAllByStateNot(Session.State.DELETED);
    }

    @Override
    public void deleteSession(Long sessionId) {
        Session sessionForDelete = sessionsRepository.findById(sessionId).orElseThrow();
        sessionForDelete.setState(Session.State.DELETED);

        sessionsRepository.save(sessionForDelete);
    }

    @Override
    public void addMoviesToSession(Long sessionId, Long movieId) {
        Session session = sessionsRepository.findById(sessionId).orElseThrow();
        Movie movie = moviesRepository.findById(movieId).orElseThrow();

        movie.getSessions().add(session);

        moviesRepository.save(movie);
    }
}