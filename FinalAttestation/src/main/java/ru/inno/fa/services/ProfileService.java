package ru.inno.fa.services;

import ru.inno.fa.models.Client;
import ru.inno.fa.security.details.CustomClientDetails;

public interface ProfileService {
    Client getCurrent(CustomClientDetails clientDetails);
}
