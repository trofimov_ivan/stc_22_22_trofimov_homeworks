package ru.inno.fa.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.fa.models.Client;
import ru.inno.fa.repositories.ClientsRepository;
import ru.inno.fa.security.details.CustomClientDetails;
import ru.inno.fa.services.ProfileService;

@RequiredArgsConstructor
@Service
public class ProfileServiceImpl implements ProfileService {

    private final ClientsRepository clientsRepository;

    @Override
    public Client getCurrent(CustomClientDetails clientDetails) {
        return clientsRepository.findById(clientDetails.getUser().getId()).orElseThrow();
    }
}
