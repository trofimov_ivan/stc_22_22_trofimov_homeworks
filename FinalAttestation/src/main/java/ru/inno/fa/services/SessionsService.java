package ru.inno.fa.services;

import ru.inno.fa.dto.SessionForm;
import ru.inno.fa.models.Movie;
import ru.inno.fa.models.Session;
import ru.inno.fa.models.Client;

import java.util.List;

public interface SessionsService {

    void addClientToSession(Long sessionId, Long clientId);

    Session getSession(Long sessionId);

    List<Client> getNotInSessionClients(Long sessionId);
    List<Movie> getNotInSessionMovies(Long sessionId);

    List<Client> getInSessionClients(Long sessionId);
    List<Movie> getInSessionMovies(Long sessionId);

    void addSession(SessionForm session);

    List<Session> getAllSessions();

    void deleteSession(Long sessionId);

    void addMoviesToSession(Long sessionId, Long movieId);
}

