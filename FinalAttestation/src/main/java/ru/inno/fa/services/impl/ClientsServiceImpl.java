package ru.inno.fa.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.inno.fa.dto.ClientForm;
import ru.inno.fa.models.Client;
import ru.inno.fa.repositories.ClientsRepository;
import ru.inno.fa.services.ClientsService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ClientsServiceImpl implements ClientsService {

    private final ClientsRepository clientsRepository;

    @Override
    public List<Client> getAllClients() {
        return clientsRepository.findAllByStateNot(Client.State.DELETED);
    }
    PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    @Override
    public void addClient(ClientForm client) {
        Client newClient = Client.builder()
                .email(client.getEmail())
                .password(passwordEncoder.encode(client.getPassword()))
                .firstName(client.getFirstName())
                .lastName(client.getLastName())
                .role(Client.Role.USER)
                .state(Client.State.CONFIRMED)
                .build();

        clientsRepository.save(newClient);
    }

    @Override
    public Client getClient(Long id) {
        return clientsRepository.findById(id).orElseThrow();
    }

    @Override
    public void updateClient(Long clientId, ClientForm client) {
        Client clientForUpdate = clientsRepository.findById(clientId).orElseThrow();

        clientForUpdate.setFirstName(client.getFirstName());
        clientForUpdate.setLastName(client.getLastName());

        clientsRepository.save(clientForUpdate);
    }

    @Override
    public void deleteClient(Long clientId) {
        Client clientForDelete = clientsRepository.findById(clientId).orElseThrow();
        clientForDelete.setState(Client.State.DELETED);

        clientsRepository.save(clientForDelete);
    }
}
