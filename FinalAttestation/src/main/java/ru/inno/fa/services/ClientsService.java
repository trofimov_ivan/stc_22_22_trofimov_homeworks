package ru.inno.fa.services;

import ru.inno.fa.dto.ClientForm;
import ru.inno.fa.models.Client;

import java.util.List;

public interface ClientsService {
    List<Client> getAllClients();

    void addClient(ClientForm client);

    Client getClient(Long id);

    void updateClient(Long clientId, ClientForm client);

    void deleteClient(Long clientId);
}
