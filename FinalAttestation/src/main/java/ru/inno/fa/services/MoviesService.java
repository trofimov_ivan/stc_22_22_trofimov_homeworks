package ru.inno.fa.services;

import ru.inno.fa.dto.MovieForm;
import ru.inno.fa.models.Movie;

import java.util.List;

public interface MoviesService {
    List<Movie> getAllMovies();

    void addMovie(MovieForm movie);

    Movie getMovie(Long id);

    void updateMovie(Long movieId, MovieForm movie);

    void deleteMovie(Long movieId);
}