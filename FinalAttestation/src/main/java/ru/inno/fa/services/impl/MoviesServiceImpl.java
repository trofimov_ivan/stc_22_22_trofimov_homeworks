package ru.inno.fa.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.fa.dto.MovieForm;
import ru.inno.fa.models.Movie;
import ru.inno.fa.repositories.MoviesRepository;
import ru.inno.fa.services.MoviesService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MoviesServiceImpl implements MoviesService {
    private final MoviesRepository moviesRepository;
    @Override
    public List<Movie> getAllMovies() {
        return moviesRepository.findAllByStateNot(Movie.State.DELETED);
    }

    @Override
    public void addMovie(MovieForm movie) {
        Movie newMovie = Movie.builder()
                .name(movie.getName())
                .description(movie.getDescription())
                .price(movie.getPrice())
                .startTime(movie.getStartTime())
                .finishTime(movie.getFinishTime())
                .state(Movie.State.CONFIRMED)
                .build();
        moviesRepository.save(newMovie);
    }

    @Override
    public Movie getMovie(Long id) {
        return moviesRepository.findById(id).orElseThrow();
    }

    @Override
    public void updateMovie(Long movieId, MovieForm updateData) {
        Movie movieForUpdate = moviesRepository.findById(movieId).orElseThrow();
        movieForUpdate.setName(updateData.getName());
        movieForUpdate.setDescription(updateData.getDescription());
        movieForUpdate.setPrice(updateData.getPrice());
        movieForUpdate.setStartTime(updateData.getStartTime());
        movieForUpdate.setFinishTime(updateData.getFinishTime());
        moviesRepository.save(movieForUpdate);
    }

    @Override
    public void deleteMovie(Long movieId) {
        Movie movieForDelete = moviesRepository.findById(movieId).orElseThrow();
        movieForDelete.setState(Movie.State.DELETED);
        moviesRepository.save(movieForDelete);
    }
}
