package ru.inno.fa.security.details;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.inno.fa.models.Client;

import java.util.Collection;
import java.util.Collections;

public class CustomClientDetails implements UserDetails {

    private Client client;

    public Client getUser() {
        return client;
    }

    public CustomClientDetails(Client client) {
        this.client = client;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        String role = client.getRole().toString();
        return Collections.singleton(new SimpleGrantedAuthority(role));
    }

    @Override
    public String getPassword() {
        return client.getPassword();
    }

    @Override
    public String getUsername() {
        return client.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !client.getState().equals(Client.State.DELETED);
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return client.getState().equals(Client.State.CONFIRMED);
    }
}
