package ru.inno.fa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.fa.models.Session;

import java.util.List;

public interface SessionsRepository extends JpaRepository<Session, Long> {
    List<Session> findAllByStateNot(Session.State state);
}
