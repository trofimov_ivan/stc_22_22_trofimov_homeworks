package ru.inno.fa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.fa.models.Movie;
import ru.inno.fa.models.Session;

import java.util.List;

public interface MoviesRepository extends JpaRepository <Movie, Long> {
    List<Movie> findAllByStateNot(Movie.State state);
    List<Movie> findAllBySessionsNotContains(Session session);

    List<Movie> findAllBySessionsContains(Session session);
}
