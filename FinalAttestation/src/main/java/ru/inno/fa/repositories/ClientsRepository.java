package ru.inno.fa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.fa.models.Client;
import ru.inno.fa.models.Session;

import java.util.List;
import java.util.Optional;

public interface ClientsRepository extends JpaRepository<Client, Long> {
    List<Client> findAllByStateNot(Client.State state);
    List<Client> findAllBySessionsNotContains(Session session);

    List<Client> findAllBySessionsContains(Session session);

    Optional<Client> findByEmail(String email);
}
