package ru.inno.fa.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.fa.dto.ClientForm;
import ru.inno.fa.security.details.CustomClientDetails;
import ru.inno.fa.services.ClientsService;


@RequestMapping("/clients")
@Controller
@RequiredArgsConstructor
public class
ClientsController {
    private final ClientsService clientsService;

    @GetMapping
    public String getClientsPage(@AuthenticationPrincipal CustomClientDetails customClientDetails, @RequestParam(value = "orderBy", required = false) String orderBy,
                                 @RequestParam(value = "dir", required = false) String direction, Model model) {
        model.addAttribute("role", customClientDetails.getUser().getRole());
        model.addAttribute("clients", clientsService.getAllClients());
        return "clients/clients_page";
    }

    @PostMapping
    public String addClient(ClientForm client) {
        clientsService.addClient(client);
        return "redirect:/clients";
    }

    @GetMapping("/{client-id}")
    public String getClientPage(@PathVariable("client-id") Long id, Model model) {
        model.addAttribute("client", clientsService.getClient(id));
        return "clients/client_page";
    }

    @PostMapping("/{client-id}/update")
    public String updateClient(@PathVariable("client-id") Long clientId, ClientForm client) {
        clientsService.updateClient(clientId, client);
        return "redirect:/clients/" + clientId;
    }

    @GetMapping("/{client-id}/delete")
    public String updateClient(@PathVariable("client-id") Long clientId) {
        clientsService.deleteClient(clientId);
        return "redirect:/clients";
    }
}
