package ru.inno.fa.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.fa.dto.MovieForm;
import ru.inno.fa.security.details.CustomClientDetails;
import ru.inno.fa.services.MoviesService;

@RequestMapping("/movies")
@Controller
@RequiredArgsConstructor
public class MoviesController {
    private final MoviesService moviesService;

    @GetMapping
    public String getMoviesPage(@AuthenticationPrincipal CustomClientDetails customClientDetails,
                                @RequestParam(value = "orderBy", required = false) String orderBy,
                                @RequestParam(value = "dir", required = false) String direction, Model model) {
        model.addAttribute("movies", moviesService.getAllMovies());
        model.addAttribute("role", customClientDetails.getUser().getRole());
        return "movies/movies_page";
    }

    @PostMapping
    public String addMovie(MovieForm movie) {
        moviesService.addMovie(movie);
        return "redirect:/movies";
    }

    @GetMapping("/{movie-id}")
    public String getMoviePage(@AuthenticationPrincipal CustomClientDetails customClientDetails,
                               @PathVariable("movie-id") Long id, Model model) {
        model.addAttribute("movie", moviesService.getMovie(id));
        model.addAttribute("role", customClientDetails.getUser().getRole());
        return "movies/movie_page";
    }

    @PostMapping("/{movie-id}/update")
    public String updateClient(@PathVariable("movie-id") Long movieId, MovieForm movie) {
        moviesService.updateMovie(movieId, movie);
        return "redirect:/movies/" + movieId;
    }

    @GetMapping("/{movie-id}/delete")
    public String updateMovie(@PathVariable("movie-id") Long movieId) {
        moviesService.deleteMovie(movieId);
        return "redirect:/movies";
    }
}
