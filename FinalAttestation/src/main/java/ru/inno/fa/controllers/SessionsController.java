package ru.inno.fa.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.fa.dto.SessionForm;
import ru.inno.fa.security.details.CustomClientDetails;
import ru.inno.fa.services.SessionsService;

@RequiredArgsConstructor
@Controller
@RequestMapping("/sessions")
public class SessionsController {

    private final SessionsService sessionsService;

    @PostMapping
    public String addSession(SessionForm session) {
        sessionsService.addSession(session);
        return "redirect:/sessions";
    }
    @GetMapping
    public String getSessionPage(@AuthenticationPrincipal CustomClientDetails customClientDetails, Model model) {
        model.addAttribute("sessions", sessionsService.getAllSessions());
        model.addAttribute("role", customClientDetails.getUser().getRole());
        return "sessions/sessions_page";
    }

    @GetMapping("/{session-id}/delete")
    public String updateSession(@PathVariable("session-id") Long sessionId) {
        sessionsService.deleteSession(sessionId);
        return "redirect:/sessions";
    }

    @PostMapping("/{session-id}/clients")
    public String addClientToSession(@PathVariable("session-id") Long sessionId,
                                     @RequestParam("client-id") Long clientId) {
        sessionsService.addClientToSession(sessionId, clientId);
        return "redirect:/sessions/" + sessionId;
    }
    @PostMapping("/{session-id}/movies")
    public String addMoviesToSession(@PathVariable("session-id") Long sessionId,
                                     @RequestParam("movie-id") Long movieId) {
        sessionsService.addMoviesToSession(sessionId, movieId);
        return "redirect:/sessions/" + sessionId;
    }
    @GetMapping("/{session-id}")
    public String getSessionPage(@AuthenticationPrincipal CustomClientDetails customClientDetails,
                                 @PathVariable("session-id") Long sessionId, Model model,
                                 @PathVariable("session-id") String parameter) {
        model.addAttribute("session", sessionsService.getSession(sessionId));
        model.addAttribute("notInSessionClients", sessionsService.getNotInSessionClients(sessionId));
        model.addAttribute("inSessionClients", sessionsService.getInSessionClients(sessionId));
        model.addAttribute("notInSessionMovies", sessionsService.getNotInSessionMovies(sessionId));
        model.addAttribute("inSessionMovies", sessionsService.getInSessionMovies(sessionId));
        model.addAttribute("role", customClientDetails.getUser().getRole());
        return "sessions/session_page";
    }

}



