package ru.inno.homework.services;

import ru.inno.homework.dto.LessonForm;

import ru.inno.homework.models.Lesson;

import java.util.List;

public interface LessonsService {
    List<Lesson> getAllLessons();

    void addLesson(LessonForm lesson);

    Lesson getLesson(Long id);

    void updateLesson(Long lessonId, LessonForm lesson);

    void deleteLesson(Long lessonId);
}
