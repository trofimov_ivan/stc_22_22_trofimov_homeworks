package ru.inno.homework.services;

import ru.inno.homework.dto.CourseForm;
import ru.inno.homework.models.Course;
import ru.inno.homework.models.Lesson;
import ru.inno.homework.models.User;

import java.util.List;

public interface CoursesService {

    void addStudentToCourse(Long courseId, Long studentId);

    Course getCourse(Long courseId);

    List<User> getNotInCourseStudents(Long courseId);
    List<Lesson> getNotInCourseLessons(Long courseId);

    List<User> getInCourseStudents(Long courseId);
    List<Lesson> getInCourseLessons(Long courseId);

    void addCourse(CourseForm course);

    List<Course> getAllCourses();

    void deleteCourse(Long courseId);

    void addLessonsToCourse(Long courseId, Long lessonId);
}

