package ru.inno.homework.services;

import ru.inno.homework.dto.UserForm;
import ru.inno.homework.models.User;

import java.util.List;

public interface UsersService {
    List<User> getAllUsers();

    void addUser(UserForm user);

    User getUser(Long id);

    void updateUser(Long userId, UserForm user);

    void deleteUser(Long userId);
}
