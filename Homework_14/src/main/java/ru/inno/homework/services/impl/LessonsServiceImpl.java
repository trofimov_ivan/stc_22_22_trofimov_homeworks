package ru.inno.homework.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.homework.dto.LessonForm;
import ru.inno.homework.models.Lesson;
import ru.inno.homework.repositories.LessonsRepository;
import ru.inno.homework.services.LessonsService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LessonsServiceImpl implements LessonsService {
    private final LessonsRepository lessonsRepository;
    @Override
    public List<Lesson> getAllLessons() {
        return lessonsRepository.findAllByStateNot(Lesson.State.DELETED);
    }

    @Override
    public void addLesson(LessonForm lesson) {
        Lesson newLesson = Lesson.builder()
                .name(lesson.getName())
                .startTime(lesson.getStartTime())
                .finishTime(lesson.getFinishTime())
                .state(Lesson.State.CONFIRMED)
                .build();
        lessonsRepository.save(newLesson);
    }

    @Override
    public Lesson getLesson(Long id) {
        return lessonsRepository.findById(id).orElseThrow();
    }

    @Override
    public void updateLesson(Long lessonId, LessonForm updateData) {
        Lesson lessonForUpdate = lessonsRepository.findById(lessonId).orElseThrow();
        lessonForUpdate.setName(updateData.getName());
        lessonForUpdate.setStartTime(updateData.getStartTime());
        lessonForUpdate.setFinishTime(updateData.getFinishTime());
        lessonsRepository.save(lessonForUpdate);
    }

    @Override
    public void deleteLesson(Long lessonId) {
        Lesson lessonForDelete = lessonsRepository.findById(lessonId).orElseThrow();
        lessonForDelete.setState(Lesson.State.DELETED);
    lessonsRepository.save(lessonForDelete);
    }
}
