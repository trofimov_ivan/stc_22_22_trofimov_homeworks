package ru.inno.homework.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.homework.dto.CourseForm;
import ru.inno.homework.models.Course;
import ru.inno.homework.models.Lesson;
import ru.inno.homework.models.User;
import ru.inno.homework.repositories.CoursesRepository;
import ru.inno.homework.repositories.LessonsRepository;
import ru.inno.homework.repositories.UsersRepository;
import ru.inno.homework.services.CoursesService;

import java.util.List;

@RequiredArgsConstructor
@Service
public class CoursesServiceImpl implements CoursesService {
    private final CoursesRepository coursesRepository;
    private final UsersRepository usersRepository;
    private final LessonsRepository lessonsRepository;
    @Override
    public void addStudentToCourse(Long courseId, Long studentId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        User student = usersRepository.findById(studentId).orElseThrow();

        student.getCourses().add(course);

        usersRepository.save(student);

    }

    @Override
    public Course getCourse(Long courseId) {
        return coursesRepository.findById(courseId).orElseThrow();
    }

    @Override
    public List<User> getNotInCourseStudents(Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        return usersRepository.findAllByCoursesNotContains(course);
    }

    @Override
    public List<Lesson> getNotInCourseLessons(Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        return lessonsRepository.findAllByCoursesNotContains(course);
    }

    @Override
    public List<User> getInCourseStudents(Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        return usersRepository.findAllByCoursesContains(course);

    }

    @Override
    public List<Lesson> getInCourseLessons(Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        return lessonsRepository.findAllByCoursesContains(course);
    }

    @Override
    public void addCourse(CourseForm course) {
        Course newCourse = Course.builder()
                .title(course.getTitle())
                .start(course.getStart())
                .finish(course.getFinish())
                .state(Course.State.CONFIRMED)
                .build();

        coursesRepository.save(newCourse);
    }

    @Override
    public List<Course> getAllCourses() {
        return coursesRepository.findAllByStateNot(Course.State.DELETED);
    }

    @Override
    public void deleteCourse(Long courseId) {
        Course courseForDelete = coursesRepository.findById(courseId).orElseThrow();
        courseForDelete.setState(Course.State.DELETED);

        coursesRepository.save(courseForDelete);
    }

    @Override
    public void addLessonsToCourse(Long courseId, Long lessonId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        Lesson lesson = lessonsRepository.findById(lessonId).orElseThrow();

        lesson.getCourses().add(course);

        lessonsRepository.save(lesson);
    }
}
