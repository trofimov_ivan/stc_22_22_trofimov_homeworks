package ru.inno.homework.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.homework.dto.LessonForm;
import ru.inno.homework.services.LessonsService;

@RequestMapping("/lessons")
@Controller
@RequiredArgsConstructor
public class LessonsController {
    private final LessonsService lessonsService;

    @GetMapping
    public String getLessonsPage(@RequestParam(value = "orderBy", required = false) String orderBy,
                               @RequestParam(value = "dir", required = false) String direction, Model model) {
        model.addAttribute("lessons", lessonsService.getAllLessons());
        return "lessons/lessons_page";
    }

    @PostMapping
    public String addLesson(LessonForm lesson) {
        lessonsService.addLesson(lesson);
        return "redirect:/lessons";
    }

    @GetMapping("/{lesson-id}")
    public String getLessonPage(@PathVariable("lesson-id") Long id, Model model) {
        model.addAttribute("lesson", lessonsService.getLesson(id));
        return "lessons/lesson_page";
    }

    @PostMapping("/{lesson-id}/update")
    public String updateUser(@PathVariable("lesson-id") Long lessonId, LessonForm lesson) {
        lessonsService.updateLesson(lessonId, lesson);
        return "redirect:/lessons/" + lessonId;
    }

    @GetMapping("/{lesson-id}/delete")
    public String updateLesson(@PathVariable("lesson-id") Long lessonId) {
        lessonsService.deleteLesson(lessonId);
        return "redirect:/lessons";
    }
}
