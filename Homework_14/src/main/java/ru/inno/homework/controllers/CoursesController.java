package ru.inno.homework.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.homework.dto.CourseForm;
import ru.inno.homework.services.CoursesService;

@RequiredArgsConstructor
@Controller
@RequestMapping("/courses")
public class CoursesController {

    private final CoursesService coursesService;

    @PostMapping
    public String addCourse(CourseForm course) {
        coursesService.addCourse(course);
        return "redirect:/courses";
    }
    @GetMapping
    public String getCoursesPage(@RequestParam(value = "orderBy", required = false) String orderBy,
                               @RequestParam(value = "dir", required = false) String direction, Model model) {
        model.addAttribute("courses", coursesService.getAllCourses());
        return "courses/courses_page";
    }

    @GetMapping("/{course-id}/delete")
    public String updateCourse(@PathVariable("course-id") Long courseId) {
        coursesService.deleteCourse(courseId);
        return "redirect:/courses";
    }

    @PostMapping("/{course-id}/students")
    public String addStudentToCourse(@PathVariable("course-id") Long courseId,
                                     @RequestParam("student-id") Long studentId) {
        coursesService.addStudentToCourse(courseId, studentId);
        return "redirect:/courses/" + courseId;
    }
    @PostMapping("/{course-id}/lessons")
    public String addLessonsToCourse(@PathVariable("course-id") Long courseId,
                                     @RequestParam("lesson-id") Long lessonId) {
        coursesService.addLessonsToCourse(courseId, lessonId);
        return "redirect:/courses/" + courseId;
    }
    @GetMapping("/{course-id}")
    public String getCoursePage(@PathVariable("course-id") Long courseId, Model model) {
        model.addAttribute("course", coursesService.getCourse(courseId));
        model.addAttribute("notInCourseStudents", coursesService.getNotInCourseStudents(courseId));
        model.addAttribute("inCourseStudents", coursesService.getInCourseStudents(courseId));
        model.addAttribute("notInCourseLessons", coursesService.getNotInCourseLessons(courseId));
        model.addAttribute("inCourseLessons", coursesService.getInCourseLessons(courseId));
        return "courses/course_page";
    }

}



