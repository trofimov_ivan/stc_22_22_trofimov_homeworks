package ru.inno.homework.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.homework.dto.UserForm;
import ru.inno.homework.services.UsersService;


@RequestMapping("/users")
@Controller
@RequiredArgsConstructor
public class UsersController {
    private final UsersService usersService;

    @GetMapping
    public String getUsersPage(@RequestParam(value = "orderBy", required = false) String orderBy,
                               @RequestParam(value = "dir", required = false) String direction, Model model) {
        model.addAttribute("users", usersService.getAllUsers());
        return "users/users_page";
    }

    @PostMapping
    public String addUser(UserForm user) {
        usersService.addUser(user);
        return "redirect:/users";
    }

    @GetMapping("/{user-id}")
    public String getUserPage(@PathVariable("user-id") Long id, Model model) {
        model.addAttribute("user", usersService.getUser(id));
        return "users/user_page";
    }

    @PostMapping("/{user-id}/update")
    public String updateUser(@PathVariable("user-id") Long userId, UserForm user) {
        usersService.updateUser(userId, user);
        return "redirect:/users/" + userId;
    }

    @GetMapping("/{user-id}/delete")
    public String updateUser(@PathVariable("user-id") Long userId) {
        usersService.deleteUser(userId);
        return "redirect:/users";
    }
}
