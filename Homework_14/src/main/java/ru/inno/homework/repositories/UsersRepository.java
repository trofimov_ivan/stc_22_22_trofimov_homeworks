package ru.inno.homework.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.homework.models.Course;
import ru.inno.homework.models.User;

import java.util.List;

public interface UsersRepository extends JpaRepository<User, Long> {
    List<User> findAllByStateNot(User.State state);
    List<User> findAllByCoursesNotContains(Course course);

    List<User> findAllByCoursesContains(Course course);

}
