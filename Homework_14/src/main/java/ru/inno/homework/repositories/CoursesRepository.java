package ru.inno.homework.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.homework.models.Course;


import java.util.List;

public interface CoursesRepository extends JpaRepository<Course, Long> {
    List<Course> findAllByStateNot(Course.State state);
}
