package ru.inno.homework.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.homework.models.Course;
import ru.inno.homework.models.Lesson;


import java.util.List;

public interface LessonsRepository extends JpaRepository <Lesson, Long> {
    List<Lesson> findAllByStateNot(Lesson.State state);
    List<Lesson> findAllByCoursesNotContains(Course course);

    List<Lesson> findAllByCoursesContains(Course course);
}
