package ru.inno.homework.models;


import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.Set;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(exclude = {"lessons", "students"})
public class Course {
    public enum State {
         CONFIRMED, DELETED
    }
    @Enumerated(value = EnumType.STRING)
    private Course.State state;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    private String title;

    @Column(length = 1000)
    private String description;

    private LocalDate start;
    private LocalDate finish;

    @OneToMany(mappedBy = "course", fetch = FetchType.EAGER)
    private Set<Lesson> lessons;

    @ManyToMany(mappedBy = "courses", fetch = FetchType.EAGER)
    private Set<User> students;
}
