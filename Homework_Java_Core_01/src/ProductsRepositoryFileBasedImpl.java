import java.util.List;
import java.io.*;
import java.util.Objects;
import java.util.function.Function;
import java.io.IOException;

public class ProductsRepositoryFileBasedImpl implements ProductsRepostory {
    private final String fileName;

    private static final Function<String, Product> stringToProductMapper = currentProduct -> {
        String[] parts = currentProduct.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String name = parts[1];
        Double price = Double.valueOf(parts[2]);
        Integer quantity = Integer.parseInt(parts[3]);
        return new Product(id, name, price, quantity);
    };

    private static final Function<Product, String> productToStringMapper = product ->
            product.getId().toString() + "|" + product.getName() + "|" + product.getPrice() + "|"
                    + product.getQuantity();

    public ProductsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public Product findById(Integer id) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getId().equals(id))
                    .findFirst()
                    .get();
        } catch (IOException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getName().toLowerCase().contains(title.toLowerCase()))
                    .toList();
        } catch (IOException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public void update(Product product) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            List<Product> productList = reader.lines().map(stringToProductMapper).toList();
            Product oldProduct = productList.stream().filter(it -> it.getId().equals(product.getId()))
                    .findFirst().get();
            Product newProduct = new Product(oldProduct.getId(), product.getName(),
                    product.getPrice(), product.getQuantity());
            List<Product> products = productList.stream().map(it -> {
                if (Objects.equals(it.getId(), newProduct.getId())) {
                    return newProduct;
                }
                return it;
            }).toList();
            saveALL(products);
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    public void saveALL(List<Product> products) {
        try (FileWriter fileWriter = new FileWriter(fileName, false);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            StringBuilder stringsProduct = new StringBuilder();
            for (Product product : products) {
                stringsProduct.append(productToStringMapper.apply(product)).append("\n");
            }
            bufferedWriter.write(stringsProduct.toString());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }
}