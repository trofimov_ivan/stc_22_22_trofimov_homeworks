public class Main {
    public static void main(String[] args) {
        ProductsRepostory productsRepostory = new ProductsRepositoryFileBasedImpl("input.txt");
        System.out.println(productsRepostory.findById(5));
        System.out.println(productsRepostory.findAllByTitleLike("ол"));

        try {
            Product milk = productsRepostory.findById(1);
            System.out.println(productsRepostory.findById(1));
            milk.setPrice(40.1);
            milk.setQuantity(10);
            productsRepostory.update(milk);
            System.out.println(productsRepostory.findById(1));
        } catch (UnsuccessfulWorkWithFileException ex) {
            System.out.println("Error");
        }
    }
}