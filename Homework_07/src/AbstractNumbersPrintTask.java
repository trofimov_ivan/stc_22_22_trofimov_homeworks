public abstract class AbstractNumbersPrintTask implements Task {
    public int from;
    public int to;
    @Override
    public abstract void complete();
    public AbstractNumbersPrintTask(int from, int to) {
        this.from = from;
        this.to = to;
    }
}
