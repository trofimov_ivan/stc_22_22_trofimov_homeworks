import java.lang.reflect.Array;
import java.util.Arrays;

public class ArrayTasksResolver {
    static void resolveTask(int[] array, ArrayTask task, int from, int to) {
        System.out.println("исходный массив " + Arrays.toString(array));
        System.out.println("Значения: from = " + from + ", to = " + to);
        System.out.println("Результат: " + task.resolve(array, from, to));
    }
}