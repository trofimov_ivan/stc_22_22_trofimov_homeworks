import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        ArrayTask sum = (array, from, to) -> {
            int Sum = 0;
            for (int i = from; i <= to; i++) {
                Sum += array[i];
            }
            return Sum;
        };
        ArrayTask sumMaxNumber = (array, from, to) -> {
            int max = array[from];
            for (int i = from; i <= to; i++) {
                if (array[i] > max) {
                    max = array[i];
                }
            }
            for (int i = from; i <= to; i++) {
                System.out.println(array[i]);
            }
            System.out.println("MAX = " + max);
            int sumMax = 0, rem;
            while (max > 0) {
                rem = max % 10;
                sumMax += rem;
                max /= 10;
            }
            return sumMax;
        };
    int array[] = {12, 62, 4, 2, 100, 40, 56};
        ArrayTasksResolver.resolveTask(array, sum, 0, 6);
        ArrayTasksResolver.resolveTask(array, sumMaxNumber, 1, 3);
}
}