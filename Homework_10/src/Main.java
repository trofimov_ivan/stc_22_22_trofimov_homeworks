import java.util.HashMap;
import java.util.Map;


public class Main {
    public static void main(String[] args) {
        String[] strings = "Hello bye Hello bye bye Hello Inno".split("\\ ");
        Map<String, Integer> map = new HashMap<>();

        for (String str : strings) {
            int count = map.getOrDefault(str, 0) + 1;
            map.put(str, count);
        }
        Map.Entry<String, Integer> maxEntry = null;
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (maxEntry == null || entry.getValue() > maxEntry.getValue()) {
                maxEntry = entry;
            }
        }
        System.out.println(maxEntry);
    }
}


